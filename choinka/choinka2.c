#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>

int main (int argc, char **argv) {
  unsigned long cnt,bnd;

  
  if (argc < 2) {
    perror("No arguments!\n");
    return EXIT_FAILURE;
  }

  cnt = atol(argv[1]);
  bnd = (cnt*(cnt+1))/2  + cnt;

  //printf("%ld -- %ld\n",cnt,bnd);
 
  char *tbl = malloc(bnd*sizeof(char));
  if (!tbl) {
    perror("No memory!\n");
    return EXIT_FAILURE;
  }
 
  memset(tbl,'*',bnd);
  unsigned long i = 1;  
  
  for(unsigned long l = 1; l <= cnt; l++) {
    tbl[i] = '\n';
    i += l + 2;   

  }
  //  tbl[bnd-1] = '\0';
  // printf(tbl);

  long long offset = 0;

  while ( (offset = write(1, tbl + offset, bnd))!=bnd ) {
  
    if (offset == -1) {
      perror("Write failed!");
      return EXIT_FAILURE;
    }  
     
    bnd -= offset;
  }


  /* if ( (wrb = write(1, tbl, bnd))!=bnd) { */
 


  /*   perror(strerror(errno)); */
  
  /*   sprintf(text,"%lld",wrb); */
  /*   perror(text); */
  /*   sprintf(text,"%ld",bnd); */
  /*   perror(text); */

  /* }  */

  free(tbl);
  return EXIT_SUCCESS;
}
