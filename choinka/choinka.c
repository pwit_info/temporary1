#include<stdlib.h>
#include<stdio.h>

int main (int argc, char **argv) {
  unsigned long cnt;

  if (argc < 2) {
    printf("No arguments!\n");
    return EXIT_FAILURE;
 
  }
  cnt = atol(argv[1]);

  printf("%ld\n",cnt);

  for(unsigned long l = 0; l < cnt; l++) {
    for(unsigned long lp = 0; lp <= l; lp++) {
      putchar('*');
    }
    putchar('\n');
  }

  return EXIT_SUCCESS;
}
