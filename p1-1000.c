/*
Task: Print numbers from 1 to 1000 without using any loop or
conditional statements. Don't just write the printf() or cout
statement 1000 times.  How would you do that using C or C++?  */

#include<stdio.h>


int nprint(int n) {

  printf("%d\n", n);
  return (n == 1000) || nprint(n + 1);
}





int main(void) {

  nprint(1);
  return 0;
}
