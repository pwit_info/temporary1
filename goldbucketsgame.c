/******************************************************************************
 GoldBucketsGame.c


Remarks: quadratic solution
*****************************************************************************/

#include<stdio.h>

#define BUCKET_NUMBER 10000

unsigned int goldbuckets[BUCKET_NUMBER];

// Name parsol stands for partial solutions.
unsigned int parsol[BUCKET_NUMBER][BUCKET_NUMBER];

//Name parsum stands for partial sums.
unsigned int parsum[BUCKET_NUMBER][BUCKET_NUMBER];

// We have computed[i][j]!=0 iff partial solution and
// partial sum for i and j are already computed.
unsigned char computed[BUCKET_NUMBER][BUCKET_NUMBER];


void reccurent_solver(unsigned int lind, unsigned int rind)
{
 
  if ( (lind > rind) || computed[lind][rind])
    return;

  if (lind == rind)
      parsum[lind][rind] = parsol[lind][rind] = goldbuckets[lind];   
  else
    {
      //First assume that we have choosen the leftmost bucket.
      reccurent_solver(lind+1,rind);
      int firstchoice  = 
	goldbuckets[lind] + (parsum[lind+1][rind] - parsol[lind+1][rind]);
      
      //Second assume that we have choosen the rightmost bucket.
      reccurent_solver(lind,rind-1);
      int secondchoice = 
	goldbuckets[rind] + (parsum[lind][rind-1] - parsol[lind][rind-1]);

      //Select the maximal solution.
      parsol[lind][rind] = (firstchoice>secondchoice)?firstchoice:secondchoice;
      //Compute partial sum.
      parsum[lind][rind] = parsum[lind+1][rind] + goldbuckets[lind];
    }


  computed[lind][rind] = 1;
}



void solve(void)
{

  // Initialize table "computed". All solutions are uncomputed
  // as for now.
  for(int i=0; i< BUCKET_NUMBER; i++)
    for(int j=0; j< BUCKET_NUMBER; j++)
      computed[i][j] = 0;

  //Then call the proper solver.
  reccurent_solver(0,BUCKET_NUMBER-1);
}


int main(void)
{

  for(int i = 0; i< BUCKET_NUMBER; i++)
    goldbuckets[i] = i+1;

  //goldbuckets == {1,2,3};

  solve();

  printf("Solution is %d:",parsol[0][BUCKET_NUMBER -1]);



  return 0;
}
