
class punkt {
private:
  double x,y;

public:
  punkt(double x, double y);
 

  void przesun(double x, double y);

  inline double wezx(void) const {return x;}
  inline double wezy(void) const {return y;}
};


class kolo {
private:
  punkt srodek;
  double r;

public:

  kolo(punkt srodek, double r);
  kolo(double x, double y, double r);

  void przesun(double x, double y);
  double pole(void) const;

  inline punkt wezSrodek(void) const {return srodek;}
  inline double wezPromien(void) const  {return r;}

};

bool wspolne(const kolo &k1, const kolo &k2);
bool wkole(punkt p, kolo k);
double dystans(const punkt &p1, const punkt &p2);
