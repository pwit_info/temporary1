#include<cmath>
#include<string>
#include<iostream>
#include "1.hpp"

punkt::punkt(double x, double y) {

  this->x = x;
  this->y = y;

}

void punkt::przesun(double x, double y) {

  this->x += x;
  this->y += y;
}



 kolo::kolo(punkt srodek, double r): srodek(srodek), r(r) {

   if (r<0) throw std::string("Promien ujemny!");

 }


kolo::kolo(double x, double y, double r): srodek(x,y), r(r) {

   if (r<0) throw std::string("Promien ujemny!");


}

void kolo::przesun(double x, double y) {

  srodek.przesun(x,y);

}


double kolo::pole(void) const {

  return M_PI*r*r;
}


double dystans(const punkt &p1, const punkt &p2) {

 
  return sqrt(pow(p1.wezx() - p2.wezx(),2) + pow(p1.wezy() - p2.wezy(),2)); 

}


bool wkole(punkt p, kolo k) {

  return dystans(p, k.wezSrodek( )) <= k.wezPromien();
}

bool wspolne(const kolo &k1, const kolo &k2) {


   return ( dystans(k1.wezSrodek(), k2.wezSrodek()) <= 
   	   k1.wezPromien() + k2.wezPromien( )); 

}
