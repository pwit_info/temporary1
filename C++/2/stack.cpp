#include "stack.hpp"
#include<iostream>

stack::stack(unsigned int size = 1 ) {

  memory = new double[size];
  this->size = size;
  firstfree = 0;
}

stack::stack(const stack &s) {

  memory = new double[s.size];
  size = s.size;
  for( firstfree = 0; firstfree < s.firstfree; firstfree++)
    memory[firstfree] = s.memory[firstfree];

}

inline bool stack::full() const {

  return (firstfree==size);
}

inline bool stack::empty() const {

  return (firstfree==0);
}



void stack::push(double val) {

  if (full()) throw std::exception();
  
  memory[firstfree++] = val;
}


double stack::pop() {

  if (empty()) throw std::exception();
  
  return memory[--firstfree];
}

double stack::top() const {

  if (empty()) throw std::exception();
  
  return memory[firstfree];

}

unsigned int stack::howMany() const {

  return firstfree;
}
