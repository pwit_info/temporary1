

class stack {

private:
  double *memory;
  unsigned int size;
  unsigned int firstfree;

  inline bool full() const;
  inline bool empty() const;

public:
  ~stack( );
  stack(unsigned int size);
  stack(const stack &s);

  void push(double val);
  double pop();
  double top() const;
  inline unsigned int howMany() const;

};
