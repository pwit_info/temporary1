/* 1.1.c*/
/* Implement an algorithm to determine if a string has all unique
   characters. What if you cannot use additional data structures? */

#include<string.h>

/* No additional data structure is used.*/
/* Time complexity: O(|string|^2)*/
/* Space complexity: O(1)*/
bool solutionA(const char  *const string) {
  
  size_t length = strlen(string);

  for (size_t i = 0; i < length; i++)
    for ( size_t j = i+1; j < length; j++) {
    
      if (string[i] == string[j]) return false;
    } 

  return true; 
}



/* Time complexity: */
/* Space complexity: */
bool solutionB(const char  *const string) {
  
  /* size_t length = strlen(string); */
  /* bool unique[ */


  /* for (size_t i = 0; i < length; i++) */
  /*   for ( size_t j = i+1; j < length; j++) { */
    
  /*     if (string[i] == string[j]) return false; */
  /*   }  */

  return true; 
}







