#include<stdio.h>

// Assume that array is sorted in non-descending order, l,r are valid
// indices in array and l<=r.
//
// Requires: for some c (l<=c<=r) array[c]==what.
// Provides: the least c1 (l<=c_1<=r), such that array[c1]==what
//
// Requires: what<array[l]. Provides: l.
//
// Requires: what>array[r]. Provides: r+1.
//
// Requires: for some c (l<=c<=c+1<=r) array[c]<what and array[c+1]>what. 
// Provides: c+1.

int binsearch(int *array, unsigned int l, unsigned int r, int what)
{
  unsigned int c ; 

  while ( l < r ) {
   c = (l + r) / 2 ;
  // c is a proper index in array
   //  if ( array[c] == what ) return c;
   if ( array[c] < what ) l = c + 1;
   else r = c;
   }

  return (what<=array[l])?l:(l+1);

}


/* int binsearch(int *array, unsigned int l, unsigned int r, int what) */
/* { */

/*   if ( what <= array[l] ) return l; */
/*   if ( what > array[r] ) return r+1; */
/*   if ( what == array[r] ) return r; */

/*   // r > l holds  */

/*   unsigned int c = (l + r) / 2 ; */
/*   // c is a proper index in array */
  
/*   if ( array[c] == what ) return c; */
/*   if ( array[c] < what ) { */

/*     return binsearch( array, c, r - 1, what); */

/*   } else { //array[c] > what */

/*     return binsearch( array, l, c, what);   */

/*   } */
 
/* } */



int main(void) {
  int arr[] = {1,2,2,2,2,5};
  unsigned maxi = sizeof(arr)/sizeof(int) -1;  
  int elem = 8;
  
  for(elem = -10; elem < 100; elem++)
    printf("The index of %d is %d\n",elem, binsearch(arr,0,maxi,elem)); 
  

  return 0;

}

